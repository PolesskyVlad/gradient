$(document).ready(function() {
    $('a[href^="#"]').on('click', function (event) {
        event.preventDefault();
        var id = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(id).offset().top
        }, 2000);
    });
    $('.slider').slick({
        dots: true,
        nextArrow: '.slick-next',
        prevArrow: '.slick-prev',
        slidesToShow: 3,
        appendDots: $('.slick-dots'),
        autoplay: true,
        mobileFirst:true,
        variableWidth: true,
        centerMode: true,
        responsive: [
        {
            breakpoint: 1500,
            settings: {
                variableWidth: false,
                centerMode: false,
            }
        }]
    });
    $('.ham').on('click', function () {
       $('.mobile-nav').toggleClass('open');
       $('body').toggleClass('hidden');
    });
    $('.mobile-nav a').on('click', function () {
       $('.ham').removeClass('active');
       $('.mobile-nav').removeClass('open');
        $('body').removeClass('hidden');
    });
});
setTimeout(function () {
    $('.animation__first-screen').addClass('hidden');
}, 2000);
setTimeout(function () {
    $('.header').addClass('slate');
}, 4000);


$(window).on('scroll', function(){
    $('.animation__first-screen').hide();
    $('.header').addClass('slate');
});

$(window).on('resize scroll', function(){
    window.FINALIZE_GRADIENT_ANIMATION({
        width: window.innerWidth,
        height: window.innerHeight,
        offsetFromTopInPixels: getOffsetFromTopInPixels(),
    });
});

$('#subscribe-form').submit(function (e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    location.reload();
});

$('#contact-us-form').submit(function (e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    location.reload();
});
